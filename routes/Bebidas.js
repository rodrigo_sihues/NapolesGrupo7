'use strict'

var express = require('express');
var BebidasController = require('../controllers/Bebidas');
var router = express.Router();

// RUTAS PARA PRODUCTO
router.get('/bebidas/list', BebidasController.listar);
router.get('/bebidas/form/:id', BebidasController.form);// ABRIR FORMULARIO
router.post('/bebidas/save', BebidasController.save);// GUARDAR

module.exports = router;