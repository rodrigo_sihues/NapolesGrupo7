'use strict'

var express = require('express');
var CombosController = require('../controllers/Combos');
var router = express.Router();

// RUTAS PARA PRODUCTO
router.get('/combos/list', CombosController.listar);
router.get('/combos/form/:id', CombosController.form);// ABRIR FORMULARIO
router.post('/combos/save', CombosController.save);// GUARDAR

module.exports = router;