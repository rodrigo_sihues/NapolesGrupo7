'use strict'

var express = require('express');
var ExtrasController = require('../controllers/Extras');
var router = express.Router();

// RUTAS PARA MARCA
router.get('/extras/list', ExtrasController.listar);
router.get('/extras/form/:id', ExtrasController.form);// ABRIR FORMULARIO
router.post('/extras/save', ExtrasController.save);// GUARDAR

module.exports = router;