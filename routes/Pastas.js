'use strict'

var express = require('express');
var PastasController = require('../controllers/Pastas');
var router = express.Router();

// RUTAS PARA PRODUCTO
router.get('/pastas/list', PastasController.listar);
router.get('/pastas/form/:id', PastasController.form);// ABRIR FORMULARIO
router.post('/pastas/save', PastasController.save);// GUARDAR

module.exports = router;