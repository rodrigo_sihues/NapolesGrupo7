'use strict'

var express = require('express');
var PizzasController = require('../controllers/Pizza');
var router = express.Router();

// RUTAS PARA PRODUCTO
router.get('/pizza/list', PizzasController.listar);
router.get('/pizza/form/:id', PizzasController.form);// ABRIR FORMULARIO
router.post('/pizza/save', PizzasController.save);// GUARDAR

module.exports = router;