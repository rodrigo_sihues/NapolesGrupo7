'use strict'

// REQUIRES
var express = require('express');

// CARGAR ARCHIVOS DE RUTAS
var Inicio_routes = require('./routes/Inicio');
var Pizza_routes = require('./routes/Pizza');
var Pastas_routes = require('./routes/Pastas');
var Bebidas_routes = require('./routes/Bebidas');
var Extras_routes = require('./routes/Extras');
var Combos_routes = require('./routes/Combos');

// EJECUTAR EXPRESS
var app = express();

// ASIGNO EJS A LAS VISTAS
app.set('view engine','ejs');

// DECODIFICACION DE ENVIOS FORM
app.use(express.urlencoded({extended:false}));
app.use(express.json());

// REESCRIBIR RUTAS
app.use('/',Inicio_routes);
app.use('/',Pastas_routes);
app.use('/',Pizza_routes);
app.use('/',Bebidas_routes);
app.use('/',Extras_routes);
app.use('/',Combos_routes);

//EXPORTAR MODULE
module.exports = app;