'use strict'

var mongoose = require('mongoose');
var app = require('./app');   //IMPORTAMOS EL ARCHIVO APP
const user = 'rodrigosy';
const pass = 'rodrigosy';
const uri = `mongodb://${user}:${pass}@clusterrs-shard-00-00.cok8o.mongodb.net:27017,clusterrs-shard-00-01.cok8o.mongodb.net:27017,clusterrs-shard-00-02.cok8o.mongodb.net:27017/napolespizzeria?ssl=true&replicaSet=atlas-20s7uf-shard-0&authSource=admin&retryWrites=true&w=majority`;

// PUERTO SERVIDOR
var port = process.env.port || 3999;

mongoose.Promise = global.Promise;
mongoose.connect(uri)
  .then(() => {
    console.log('La conexión a mongodb fue exitosa.');
    app.listen(port, () => { console.log('El sevidor http://localhost:3999 esta funcionando.'); });
  })
  .catch(error => console.log(error));