'use strict'

var client = require("../database/db");
var db = client.db("NapolesPizzeria");// SELECCIONANDO LA BASE DE DATOS

var controller = {
    listar: function (req, res) {
        console.log("-------------------");
        console.log("ENTRANDO A LA FUNCION LISTAR");
        db.collection("Pastas")
            .find()
            .toArray()
            .then(pastas => { res.render('pastas_list', { dataPastas: pastas }); })
            .catch(error => console.log(error))
    },
    form: function (req, res) {
        console.log("-------------------");
        console.log("ENTRANDO A LA FUNCION FORM");
        console.log("id:" + req.params.id);
        if (req.params.id == 0) {
            console.log("ENTRANDO A LA FUNCION FORM NUEVO");
            var pastas = {}
            pastas.idPastas = 0;
            pastas.pastas = "";
            pastas.descripcion = "";
            pastas.precio = "";
            res.render('pastas_form', { pastasForm: pastas });
        }
    },
    save: function (req, res) {
        console.log("-------------------");
        console.log("ENTRANDO A LA FUNCION SAVE");
        console.log(req.body);
        if (req.body.idPastas == 0) {// CUANDO ES NUEVO
            db.collection("Pastas")
                .count()
                .then( // OBTENER CUANTOS PRODUCTOS TENGO
                    countPastas => { // OBTENER CUANTOS PRODUCTOS EXISTEN
                        var pastas = {}
                        pastas.idPastas = countPastas + 1;// NUEVO ID AUMENTA EN 1
                        pastas.pastas = req.body.pastas;
                        pastas.descripcion = req.body.descripcion;
                        pastas.precio = req.body.precio;
                        console.log(pastas);
                        db.collection("Pastas")
                            .insertOne(pastas)
                            .then(() => { res.redirect('/pastas/list'); })
                            .catch(error => console.log(error))
                    }
                )
        };
    }
}

module.exports = controller;