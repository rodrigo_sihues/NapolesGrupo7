'use strict'

var client = require("../database/db");
var db = client.db("NapolesPizzeria");// SELECCIONANDO LA BASE DE DATOS

var controller = {
    listar: function (req, res) {
        console.log("-------------------");
        console.log("ENTRANDO A LA FUNCION LISTAR");
        db.collection("Bebidas")
            .find()
            .toArray()
            .then(bebidas => { res.render('bebidas_list', { dataBebidas: bebidas }); })
            .catch(error => console.log(error))
    },
    form: function (req, res) {
        console.log("-------------------");
        console.log("ENTRANDO A LA FUNCION FORM");
        console.log("id:" + req.params.id);
        if (req.params.id == 0) {
            console.log("ENTRANDO A LA FUNCION FORM NUEVO");
            var bebidas = {}
            bebidas.idBebidas = 0;
            bebidas.nomBebida = "";
            bebidas.precio = "";
            res.render('bebidas_form', { bebidasForm: bebidas });
        }
    },
    save: function (req, res) {
        console.log("-------------------");
        console.log("ENTRANDO A LA FUNCION SAVE");
        console.log(req.body);
        if (req.body.idBebidas == 0) {// CUANDO ES NUEVO
            db.collection("Bebidas")
                .count()
                .then( // OBTENER CUANTOS PRODUCTOS TENGO
                    countBebidas => { // OBTENER CUANTOS PRODUCTOS EXISTEN
                        var bebidas = {}
                        bebidas.idBebidas = countBebidas + 1;// NUEVO ID AUMENTA EN 1
                        bebidas.nomBebida = req.body.nomBebida;
                        bebidas.precio = req.body.precio;
                        console.log(bebidas);
                        db.collection("Bebidas")
                            .insertOne(bebidas)
                            .then(() => { res.redirect('/bebidas/list'); })
                            .catch(error => console.log(error))
                    }
                );
        }
    }
}

module.exports = controller;