'use strict'

var client = require("../database/db");
var db = client.db("NapolesPizzeria");// SELECCIONANDO LA BASE DE DATOS

var controller = {
    listar: function (req, res) {
        console.log("-------------------");
        console.log("ENTRANDO A LA FUNCION LISTAR");
        db.collection("Combos")
            .find()
            .toArray()
            .then(combos => { res.render('combos_list', { dataCombos: combos }); })
            .catch(error => console.log(error))
    },
    form: function (req, res) {
        console.log("-------------------");
        console.log("ENTRANDO A LA FUNCION FORM");
        console.log("id:" + req.params.id);
        if (req.params.id == 0) {
            console.log("ENTRANDO A LA FUNCION FORM NUEVO");
            var combos = {}
            combos.idCombos = 0;
            combos.combos = "";
            combos.descripcion = "";
            combos.precio = "";
            res.render('combos_form', { combosForm: combos });
        }
    },
    save: function (req, res) {
        console.log("-------------------");
        console.log("ENTRANDO A LA FUNCION SAVE");
        console.log(req.body);
        if (req.body.idCombos == 0) {// CUANDO ES NUEVO
            db.collection("Combos")
                .count()
                .then( // OBTENER CUANTOS PRODUCTOS TENGO
                    countCombos => { // OBTENER CUANTOS PRODUCTOS EXISTEN
                        var combos = {}
                        combos.idCombos = countCombos + 1;// NUEVO ID AUMENTA EN 1
                        combos.combos = req.body.combos;
                        combos.descripcion = req.body.descripcion;
                        combos.precio = req.body.precio;
                        console.log(combos);
                        db.collection("Combos")
                            .insertOne(combos)
                            .then(() => { res.redirect('/combos/list'); })
                            .catch(error => console.log(error))
                    }
                )
        };
    }
}

module.exports = controller;