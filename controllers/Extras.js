'use strict'

var client = require("../database/db");
var db = client.db("NapolesPizzeria");// SELECCIONANDO LA BASE DE DATOS

var controller = {
    listar: function (req, res) {
        console.log("-------------------");
        console.log("ENTRANDO A LA FUNCION LISTAR");
        db.collection("Extras")
            .find()
            .toArray()
            .then(extras => { res.render('extras_list', { dataExtras: extras }); })
            .catch(error => console.log(error))
    },
    form: function (req, res) {
        console.log("-------------------");
        console.log("ENTRANDO A LA FUNCION FORM");
        console.log("id:" + req.params.id);
        if (req.params.id == 0) {
            console.log("ENTRANDO A LA FUNCION FORM NUEVO");
            var extras = {}
            extras.idExtras = 0;
            extras.nomExtras = "";
            extras.precio = "";
            res.render('extras_form', { extrasForm: extras });
        }
    },
    save: function (req, res) {
        console.log("-------------------");
        console.log("ENTRANDO A LA FUNCION SAVE");
        console.log(req.body);
        if (req.body.idExtras == 0) {// CUANDO ES NUEVO
            db.collection("Extras")
                .count()
                .then( // OBTENER CUANTOS PRODUCTOS TENGO
                    countExtras => { // OBTENER CUANTOS PRODUCTOS EXISTEN
                        var extras = {}
                        extras.idExtras = countExtras + 1;// NUEVO ID AUMENTA EN 1
                        extras.nomExtras = req.body.nomExtras;
                        extras.precio = req.body.precio;
                        console.log(extras);
                        db.collection("Extras")
                            .insertOne(extras)
                            .then(() => { res.redirect('/extras/list'); })
                            .catch(error => console.log(error))
                    }
                );
        }
    }
}

module.exports = controller;