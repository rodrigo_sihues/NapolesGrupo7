'use strict'

var client = require("../database/db");
var db = client.db("NapolesPizzeria");// SELECCIONANDO LA BASE DE DATOS

var controller = {
    listar: function (req, res) {
        console.log("-------------------");
        console.log("ENTRANDO A LA FUNCION LISTAR");
        db.collection("Pizza")
            .find()
            .toArray()
            .then(pizza => { res.render('pizza_list', { dataPizza: pizza }); })
            .catch(error => console.log(error))
    },
    form: function (req, res) {
        console.log("-------------------");
        console.log("ENTRANDO A LA FUNCION FORM");
        console.log("id:" + req.params.id);
        if (req.params.id == 0) {
            console.log("ENTRANDO A LA FUNCION FORM NUEVO");
            var pizza = {}
            pizza.idPizza = 0;
            pizza.pizza = "";
            pizza.descripcion = "";
            pizza.precio = "";
            res.render('pizza_form', { pizzaForm: pizza });
        }
    },
    save: function (req, res) {
        console.log("-------------------");
        console.log("ENTRANDO A LA FUNCION SAVE");
        console.log(req.body);
        if (req.body.idPizza == 0) {// CUANDO ES NUEVO
            db.collection("Pizza")
                .count()
                .then( // OBTENER CUANTOS PRODUCTOS TENGO
                    countPizzas => { // OBTENER CUANTOS PRODUCTOS EXISTEN
                        var pizza = {}
                        pizza.idPizza = countPizzas + 1;// NUEVO ID AUMENTA EN 1
                        pizza.pizza = req.body.pizza;
                        pizza.descripcion = req.body.descripcion;
                        pizza.precio = req.body.precio;
                        console.log(pizza);
                        db.collection("Pizza")
                            .insertOne(pizza)
                            .then(() => { res.redirect('/pizza/list'); })
                            .catch(error => console.log(error))
                    }
                )
        };
    }
}

module.exports = controller;